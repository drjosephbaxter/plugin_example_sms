<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

namespace plugins\SMS\plugin_example_sms;

/**
* SMS plugin helper file
*
* @author Dr Joseph Baxter <joseph.baxter@nottingham.ac.uk>
* @copyright Copyright (c) 2021 onwards The University of Nottingham
*/

/**
 * SMS import plugin.
 */
class plugin_example_sms extends \plugins\plugins_sms
{
    /**
     * Name of the plugin;
     * @var string
     */
    protected $plugin = 'plugin_example_sms';

    /**
     * Language pack component.
     * @var string
     */
    protected $langcomponent = 'plugins/SMS/plugin_example_sms/plugin_example_sms';

    /**
     * Lang pack strings.
     * @var string
     */
    private $strings;

    /**
     * User running import.
     * @var integer
     */
    private $userid;

    /**
     * Name of external student management system.
     * @var string
     */
    public const SMS = 'Example';

    /**
     * Set the available land pack strings for the plugin
     */
    private function set_lang_strings()
    {
        $langpack = new \langpack();
        $this->strings = $langpack->get_all_strings($this->langcomponent);
    }

    /**
     * Is the plugin function configured
     * @param stiring $function name of function
     * @return boolean true if configured
     */
    private function is_configured($function)
    {
        $configured = $this->config->get_setting($this->plugin, 'enable_' . $function);
        if (!is_null($configured) and $configured == true) {
            return true;
        }
        return false;
    }

    /**
     * Is this plugin enabled
     * @return boolean true if enabled
     */
    private function is_enabled()
    {
        $enabledplugins = \plugin_manager::get_plugin_type_enabled('plugin_' . $this->plugin_type);
        if (in_array($this->plugin, $enabledplugins)) {
            return true;
        }
        return false;
    }

    /**
     * Constructor
     * @param mysqli $mysqli db connection
     * @param integer $userid rogo id of user running import
     */
    public function __construct($userid = 0)
    {
        parent::__construct();
        $this->set_lang_strings();
        $this->userid = $userid;
    }

    /**
     * Example calling a web service to retrieve information.
     * @param array $args any arguments to call the web service with
     * @return string
     */
    public function callws($args = array())
    {
        $url = $this->config->get_setting($this->plugin, 'url');
        $url .= '?';
        foreach ($args as $param => $value) {
            $url .=  $param . '=' . $value . '&';
        }
        $url = rtrim($url, '&');
        $timeout = $this->config->get_setting($this->plugin, 'timeout');
        // Example of curl settings.
        $options = array(CURLOPT_TIMEOUT => $timeout,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0
        );
        // Send request.
        $restful = new \restful($this->db);
        return $restful->get($url, $options);
    }

    /**
     * Get all assessments for academic session
     * @params integer $session academic session to sync assessments with
     */
    public function get_assessments($session)
    {
        /**
         * Not in this example.
         * Process assessment data from SMS
         * Sync with Rogo using \api\assessmentmanagement
         */
    }

    /**
     * Get enrolments for academic session
     * @params integer $session academic session to sync enrolments with
     * @params integer $externalid external system module id
     */
    public function get_enrolments($session = null, $externalid = null)
    {
        // Check enabled.
        if (!$this->is_enabled() or !$this->is_configured('enrolment')) {
            return;
        }
        // Specific module.
        if (!empty($externalid)) {
            $args['externalid'] = $externalid;
        }
        // Set session if not provided.
        if (empty($session)) {
            $yearutils = new \yearutils($this->config->db);
            $session = $yearutils->get_current_session();
        }
        $args = array('academic_session' => $session);
        $response = $this->callws($args);
        // 1. Parse response.
        $enrol = json_decode($response);
        // 2. Process enrolments with \api\modulemanagement
        $mm = new \api\modulemanagement($this->db);
        $smsimports = array();
        $node = 1;
        foreach ($enrol as $enroldetails) {
            $currentenrols = array();
            $details = \module_utils::get_full_details('external', $enroldetails->id, $this->db, plugin_example_sms::SMS);
            $moduleid = $details['idMod'];
            $smsimports[$moduleid]['enrolcount'] = 0;
            $smsimports[$moduleid]['enrolusers'] = '';
            $smsimports[$moduleid]['unenrolcount'] = 0;
            $smsimports[$moduleid]['unenrolusers'] = '';
            // Enrol.
            $params = array();
            $params['moduleextid'] = $enroldetails->id;
            $params['moduleextsys'] = plugin_example_sms::SMS;
            $params['session'] = $session;
            foreach ($enroldetails->users as $users) {
                $currentenrols[$enroldetails->id][] = $users->id;
                $params['studentid'] = $users->id;
                $params['attempt'] = 1;
                $params['nodeid'] = $node;
                $node++;
                $response = $mm->enrol($params, $this->userid);
                if ($response['statuscode'] === 100) {
                    $smsimports[$moduleid]['enrolcount']++;
                    $smsimports[$moduleid]['enrolusers'] .= $users->username . ',';
                }
            }
            // Unenrol.
            $params = array();
            $params['moduleextid'] = $enroldetails->id;
            $params['moduleextsys'] = plugin_example_sms::SMS;
            $params['session'] = $session;
            $membership = \module_utils::get_student_members($session, $moduleid, $this->db);
            foreach ($membership as $idx => $member) {
                if (!in_array($member['studentid'], $currentenrols[$enroldetails->id])) {
                    $params['studentid'] = $member['studentid'];
                    $params['nodeid'] = $node;
                    $response = $mm->unenrol($params, $this->userid);
                    $node++;
                    if ($response['statuscode'] === 100) {
                        $smsimports[$moduleid]['unenrolcount']++;
                        $smsimports[$moduleid]['unenrolusers'] .= $member['username'] . ',';
                    }
                }
            }
            // Update SMS import log table.
            $smsimports[$moduleid]['enrolusers'] = rtrim($smsimports[$moduleid]['enrolusers'], ',');
            $smsimports[$moduleid]['unenrolusers'] = rtrim($smsimports[$moduleid]['unenrolusers'], ',');
            if ($smsimports[$moduleid]['unenrolcount'] > 0 or $smsimports[$moduleid]['enrolcount'] > 0) {
                \module_utils::log_sms_imports(
                    $moduleid,
                    $smsimports[$moduleid]['enrolcount'],
                    $smsimports[$moduleid]['enrolusers'],
                    $smsimports[$moduleid]['unenrolcount'],
                    $smsimports[$moduleid]['unenrolusers'],
                    'Example',
                    $session,
                    $this->db
                );
            }
        }
    }


    /**
     * Update module in an academic session
     * Updates module details and enrolments
     * @params integer $externalid external system module id
     * @params integer $session academic session to sync enrolments with
     */
    public function update_module_enrolments($externalid, $session)
    {
        if (!$this->is_enabled()) {
            return;
        }
        $this->get_modules($externalid, $session);
        $this->get_enrolments($session, $externalid);
    }

    /**
     * Get faculties/schools.
     */
    public function get_faculties()
    {
        /**
         * Not in this example.
         * Process assessment data from SMS
         * Sync with Rogo using \api\facultymanagement
         */
    }

    /**
     * Get courses
     */
    public function get_courses()
    {
        /**
         * Not in this example.
         * Process assessment data from SMS
         * Sync with Rogo using \api\coursemanagement
         */
    }

    /**
     * Get modules
     * @params integer $externalid external system module id
     * @params integer $session academic session for the module
     */
    public function get_modules($externalid = null, $session = null)
    {
        /**
         * Not in this example.
         * Process assessment data from SMS
         * Sync with Rogo using \api\modulemanagement
         */
    }

    /**
     * Publish a gradebook
     * @param integer $session academic session to publish gradebook for
     */
    public function publish_gradebook($session)
    {
        /*
         * Not in this example.
         * Process grades using \Paper_utils::get_finalised_papers
         * and create \gradebook
         * i.e. write to a file, push to an API endpoint
         */
    }

    /**
     * Check if module import is supported by the plugin
     * @return array|bool import url and translation strings, false  if module import not supported
     */
    public function supports_module_import()
    {
        if ($this->is_configured('module') or $this->is_configured('enrolment')) {
            return array('url' => $this->config->get('cfg_root_path') . '/plugins/SMS/' . $this->plugin . '/admin/enrolment.php', 'blurb' => $this->strings['importmodules'], 'tooltip' => $this->strings['importmodulestooltip']);
        } else {
            return false;
        }
    }

    /**
     * Check if faculty/school import is supported by the plugin
     * @return array|bool import url and translation strings, false if faculty/school import not supported
     */
    public function supports_faculty_import()
    {
        // Not in this example.
        return false;
    }

    /**
     * Check if course import is supported by the plugin
     * @return array|bool import url and translation strings, false  if course import not supported
     */
    public function supports_course_import()
    {
        // Not in this example.
        return false;
    }

    /**
     * Check if enrolment import is supported by the plugin
     * @return array|bool false if enrolment import not supported
     */
    public function supports_enrol_import()
    {
        if ($this->is_configured('enrolment')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if assessment import is supported by the plugin
     * @return array|bool import url and translation strings, false  if assessment import not supported
     */
    public function supports_assessment_import()
    {
        // Not in this example.
        return false;
    }

    /**
     * Get name of sms
     * @return string name of sms
     */
    public function get_name()
    {
        return self::SMS;
    }
}
