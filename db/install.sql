# USER INSERT IGNORE to add settings.
INSERT IGNORE INTO config (component, setting, value, type) values ('plugin_example_sms', 'url', 'https://localhost/plugins/SMS/plugin_example_sms/dummysms/dummy.php', 'url');
INSERT IGNORE INTO config (component, setting, value, type) values ('plugin_example_sms', 'timeout', 10, 'integer');
INSERT IGNORE INTO config (component, setting, value, type) values ('plugin_example_sms', 'enable_enrolment', 1, 'boolean');
