Title: plugin_example_sms
Author: Dr Joseph Baxter <joseph.baxter@nottingham.ac.uk>
Copyright: University of Nottingham 2021 onwards
Description:

plugin_example_sms was developed for the University of Nottingham.
It is an example SMS plugin for Rogō to show the basics needed to connect to a student management system.

Installation:

1. Extract plugin_example_sms archive into the plugins/SMS directory inside Rogō.
2. Install via the plugins/index.php admin screen.
